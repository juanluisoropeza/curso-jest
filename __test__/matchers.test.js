// comparadores comunes

describe('Comparadores comunes', () => {
    const user = {
        name: "Juan Luis",
        lastname: "Oropeza"
    };
    const user_rep = {
        name: "Juan Luis",
        lastname: "Oropeza"
    };
    const user2 = {
        name: "Abigail",
        lastname: "Reyes"
    };
    test('Igualdad de elementos', () => {
        expect(user).toEqual(user_rep)
    });
    test('No son exactamente iguales', () => {
        expect(user).not.toEqual(user2)
    });
});