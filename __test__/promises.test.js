import { getDataFromApi } from '../promises';

describe('Probando promesas', () => {
    test('Realizando una peticion a una api', (done) => {
        const api = 'https://rickandmortyapi.com/api/character';
        getDataFromApi(api).then((data) => {
            // revisas que el resultado de la peticion sea mayor a cero, es decir q traiga al menos un registro
            expect(data.results.length).toBeGreaterThan(0);
            done();
        });
    });
    test('Resuelve un Hola', () => {
        return expect(Promise.resolve('Hola!')).resolves.toBe('Hola!');
    });
    test('Resuelve con un error', () => {
        return expect(Promise.reject('Error!')).rejects.toBe('Error!');
    });
});
