import { numbers } from "../numbers";

describe('Numbers', () => {
    test('Mayor que', () => {
        expect(numbers(2, 2)).toBeGreaterThan(3);
    });
    test('Mayor que o igual', () => {
        expect(numbers(2, 2)).toBeGreaterThanOrEqual(4);
    });
    test('Menor que', () => {
        expect(numbers(2, 2)).toBeLessThan(5);
    });
    test('Menor o igual que', () => {
        expect(numbers(2, 2)).toBeLessThanOrEqual(4);
    });
    // prueba para flotantes
    test('Numeros Flotantes', () => {
        expect(numbers(2.5, 1.5)).toBeCloseTo(4);
    });
});