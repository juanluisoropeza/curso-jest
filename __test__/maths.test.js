import { sumar, multiplicar, restar, dividir } from "../maths.js";

describe('Calculos matematicos', () => {
    test('Prueba de sumas', () => {
        expect(sumar(1, 1)).toBe(2);
    });
    test('Multiplicar', () => {
        expect(multiplicar(2, 15)).toBe(30);
    });
    test('Dividir', () => {
        expect(dividir(20, 5)).toBe(4);
    });
    test('Restar', () => {
        expect(restar(20, 5)).toBe(15);
    });
});
